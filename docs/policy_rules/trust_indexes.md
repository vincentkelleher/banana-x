# :fontawesome-solid-ranking-star: Trust Index

The Trust Index is a mean to have gradual measurement, ranking, on top of the Banana-x ecosystem mandatory conformity scheme.

The Trust Index $T$ is a function of several sub-indexes, all using VC ($\texttt{VC}$) as inputs. The output is a number in $\mathbb{R}$.

$$ T: T_V, T_T, T_C, T_{SM} \rightarrow \mathbb{R}$$

with each sub-index $T_i$ as $T_i: \texttt{\{VC\}} \rightarrow \mathbb{R}$


<!-- 
$$\mathbb{R}_{GaiaX} \subseteq \mathbb{R}_{domain}$$

| Rule property                                 | A domain refining Gaia-X Conformity <br>$\forall r \in \mathbb{R}_{GaiaX} \text{ and } r_{domain} \in \mathbb{R}_{domain}$ | A domain extending Gaia-X Conformity<br>$\forall r \in \mathbb{R}_{domain} \setminus \mathbb{R}_{GaiaX}$ |
|-----------------------------------------------|--------------------------------------------------------------------------------------|--------------------------------------------------------------------|
| Attribute name: $r_{name}$                           | $r_{name_{CamelCase}} \equiv r_{name_{snake\_case}}$                               | $r_{name} \notin \mathbb{R}_{GaiaX}$                             |
| Cardinality: $\lvert r \lvert$               | $\lvert r_{domain} \lvert \ge \lvert r \lvert$                                     | _no restriction_                                                   |
| Value formats: $r \rightarrow \texttt{VF}(r)$          | $\texttt{VF}(r_{domain}) \subseteq \texttt{VF}(r)$                                       | _no restriction_                                                   |
| Trust Anchors: $r \rightarrow \texttt{TA}(r)$         | $\texttt{TA}(r_{domain}) \subseteq \texttt{TA}(r)$                             | _no restriction_                                                   |
| Trusted Data Sources: $r \rightarrow \texttt{TDS}(r)$ | $\texttt{TDS}(r_{domain}) \subseteq \texttt{TDS}(r)$                 | _no restriction_                                                   |




While "Trust" is used as a building block in every Data Spaces and Federations related documents, there are two main challenges:

- There is no unique definition of Trust; Trust is context dependent.
- The market has and will always move faster than the rules

While the [Banana-X Policy Rules](index.md) provide a baseline translating European values into objectives and measurable criteria, Data Spaces and Federations need to [extend this baseline](operating_model.md#extention-by-the-ecosystems) for their own market, vertical domain and local regulations. -->


## Sub-Indexes

### Veracity

The Veracity index $T_V$ is a function of the chains of the public keys from the issuer to the Trust Anchor.

$$\forall C_k \in \{Chains\}, T_V = \sum_{k} 0.9^{lenght(C_k)-1}$$

!!! tip

    $0.9$ was empirically selected to keep a meaninful $T_V$ value even with long machine-to-machine chains.

**If** there is one chain **then** the value is 1 **else** more chains there are, higher :octicons-arrow-up-right-24: is the veracity index.

!!! example

    For a given claim, the number of signatures on that claim (root → … → leaf → VC → **subject** ← VC ← leaf ← … ← root)

    ```mermaid
    flowchart LR
        keychain10[cert #1]
        keychain20[cert #k]
        keychain30[cert #m]
        keychain40[cert #n]
        vc1(credential #1)
        vc2(credential #2)
        cs([subject])
        keychain10 -- issues --> keychain20 -- signs --> vc1 -- refers to --> cs
        keychain30 -- issues --> keychain40 -- signs --> vc2 -- refers to --> cs
    ```


**If** the issuer is the Trust Anchor **then** the value is 1 **else** longer the chain is, lower :octicons-arrow-down-right-24: is the veracity index.

!!! example

    For a given claim, the length of the signing key chain (root cert → intermediate cert → … → leaf → VC → **subject**)

    ```mermaid
    flowchart LR
        root1[root cert]
        interm1[intermediate cert #k]
        leaf1[leaf cert]
        vc1(credential #1)
        cs([subject])

        root1 -- issues --> interm1 -- issues --> leaf1 -- signs --> vc1 -- refers to --> cs
    ```



### Transparency

The Transparency index $T_T$ is a function of:

- the number of exposed optional properties of an object versus mandatory properties of the same object. This ratio $R$ is always greater or equal to 1. $R = \frac{ count(all\:properties)}{ count(mandatory\:properties)} \ge 1$ <!-- but only if you define it as "# overall properties / # mandatory properties" -->
- the shape of the graph formed by the linked claims, measuring its eccentricity and depth.

!!! example

    Example of the same offering - an API endpoint returning a fortune from the BSD packet [fortune](https://en.wikipedia.org/wiki/Fortune_(Unix)) - with an increasing :octicons-arrow-up-right-24: transparency index, $T_{T_2} > T_{T_1} > T_{T_0}$

    === "Minimun requirement $T_{T_0}$"

        ```mermaid
        flowchart TB
            so1{{Fortune teller}}
            part1[Provider 1]
            so1 -- providedBy --> part1
        ```

        **Service Offering**

        ```yaml
        name: Fortune teller
        description: API to randomly return a fortune
        providedBy: url(provider1)
        termsAndConditions:
        - https://some.url.for.terms.and.condition.example.com
        ```

        **Provider 1**

        ```yaml
        registrationNumber: FR5910.899103360
        headquarterAddress:
        country: FR
        legalAddress:
        country: FR
        ```

    === "Improved requirement $T_{T_1} > T_{T_0}$"

        ```mermaid
        flowchart TB
            so1{{Fortune teller}}
            part1[Provider 1]
            vr1[\Software 1\]

            so1 -- providedBy --> part1
            so1 -- aggregationOf --> vr1
            vr1 -- copyrightOwnedBy --> part1 
        ```

        **Service Offering**

        ```yaml
        name: Fortune teller
        description: API to randomly return a fortune
        providedBy: url(provider1)
        aggregationOf:
        - url(software1)
        termsAndConditions:
        - https://some.url.for.terms.and.condition.example.com
        ```

        **Software 1**

        ```yaml
        name: api software
        copyrightOwnedBy:
        - url(provider1)
        license:
        - EPL-2.0
        ```

    === "Improved requirement $T_{T_2} > T_{T_1}$"

        ```mermaid
        flowchart TB
            so1{{Fortune Teller}}
            part1[Provider 1]
            part2[Participant 2]
            part3[Participant 3]
            vr1[\Software 1/]
            vr2[\DataSet\]
            pr1[/Datacenter\]

            so1 -- providedBy --> part1
            so1 -- aggregationOf --> vr1 & vr2 & pr1
            vr1 -- copyrightOwnedBy --> part1
            vr1 -- tenantOwnedByBy --> part1
            vr1 -- maintainedBy --> part1

            vr2 -- copyrightOwnedBy --> part3

            pr1 -- maintainerBy --> part2
        ```

        **Service Offering**

        ```yaml
        name: Fortune teller
        description: API to randomly return a fortune
        providedBy: url(provider1)
        aggregationOf:
        - url(software1)
        - url(dataset1)
        - url(datacenter1)
        termsAndConditions:
        - https://some.url.for.terms.and.condition.example.com
        policies:
        - type: opa
            content: |-
            package fortune
            allow = true {
                input.method = "GET"
            }
        ```

        **API 1**

        ```yaml
        name: api software
        maintainedBy:
        - url(provider1)
        tenantOwnedByBy:
        - url(provider1)
        copyrightOwnedBy:
        - url(provider1)
        license:
        - EPL-2.0
        ```

        **Dataset 1**

        ```yaml
        name: fortune dataset
        copyrightOwnedBy:
        - name: The Regents of the University of California
            registrationNumber: C0008116
            headquarterAddress:
            state: CA
            country: USA
            legalAddress:
            state: CA
            country: USA
        license:
        - BSD-3
        - https://metadata.ftp-master.debian.org/changelogs//main/f/fortune-mod/fortune-mod_1.99.1-7.1_copyright
        ```

        **Participant 2**

        ```yaml
        name: Cloud Service Provider
        registrationNumber: FR5910.424761419
        headquarterAddress:
        country: FR
        legalAddress:
        country: FR
        ```

        **Datacenter 1**

        ```yaml
        name: datacenter
        maintainedBy: url(participant2)
        location:
        - country: FR
        ```

### Composability

The Composability index $T_C$ is a function of two or more service descriptions, computing:

- the capacity of those services to be technically composed together, e.g.: software stack, compute, network and storage characteristics, plugin and extension configurations, …

This index is computed, for example, by instances of the Federated Catalogues, by analysing and comparing the characteristics of several service descriptions.

This index can be decomposed for each service description into several sub-functions:

- the level of detail of the [Software Bill of Material](https://www.cisa.gov/sbom)
- the list of known vulnerabilities such as the ones listed on the [National Vulnerability Database](https://nvd.nist.gov/).
- Intellectual Property rights via the analysis of the licenses and copyrights.
- the level of stickiness or adherence to specific known closed services
- the level of readiness for lift-and-shift migration.

Check [Fundamental/Linked Data](../fundamentals/linked_data.md) for an example.


### Semantic Match

The Semantic Match index $T_{SM}$ is a function of:

- the use of recommended vocabularies (Data Privacy Vocabulary, ODRL, …)
- the unsupervised classification of objects based on their properties
- Natural Language Processing and Large Language Model analysis from and to Domain Specific Language (DSL)
- Structured metadata information embedded in unstructured data containers (PDF/A-3a, …)


[^xacml]: PDP <https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=xacml>
