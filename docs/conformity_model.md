# generic model
Criteria aka speficiedRequirements:
    referToScheme: <ConformityAssessmentSchema>
    fullfilledBy: <ConformityAssessmentSchema>
ConformityAssessmentSchema aka Norm aka ConformityReference:
    requirementDefinition: list of Criteria[] (list de choix obligatoire) (list1 | list2 | ... ) and list1 = (criteria1 & criteria2 & ...)
    list of accreditationBodies or root publicKeys:<body1> | <body2> | ...
    assessmentMethod: 1st | 2nd | 3rd party assessment activity

# by gaia-x
GaiaXBasicConformitySchema: (signed by Gaia-X)
    Criteria: P1.1.1declaration, ...... P1.1.5 (55)
GaiaXLabelLevel1Schema: (signed by Gaia-X)
    Criteria: P1.1.1declaration, ...... P1.1.5 (60)
GaiaXLabelLevel2Schema: (signed by Gaia-X)
    Criteria: P1.1.1declaration, ...... P2.1.5
GaiaXLabelLevel3Schema: (signed by Gaia-X)
    Criteria: P1.1.1certification, ...... P5.1.2 (64)
    
# in prod
GaiaXLabelLevel1: signed by BMW
- Scheme: << GaiaXLabelLevel1Scheme signed by Gaia-X>> signed by BMW
- CriteriaP1.1.1: signed by BMW
x60
- CriteriaP1.1.5: signed by BMW
ComplianceCertificateClaim  aka GaiaXLabelLevel3-AccreditedCABforLevel3-forBMW: signed by AccreditedCABforLevel3
- Scheme: << GaiaXLabelLevel3Schema signed by Gaia-X>> signed by AccreditedCABforLevel3
- CriteriaP1.1.1: signed by AccreditedCABforLevel3
- CriteriaP1.1.5: signed by AccreditedCABforLevel3
