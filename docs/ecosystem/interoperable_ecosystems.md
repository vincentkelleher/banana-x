# :octicons-arrow-switch-16: Interoperable Ecosystems

!!! danger "One to rule them all"

    The sucess of this blueprint is the **absence** of one overarching autority ruling all parties.  
    One the opposite, this blueprint emphasis on several autonomous authorities combining their efforts and details how to achieve interoperability.

```kroki-plantuml
@startuml Ecosystem Authorities
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Context.puml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml

System(eco1, "Ecosystem 🍌")
System(eco2, "Ecosystem 🍍")
System(econ, "Ecosystem 🥥")
System(econ1, "Ecosystem 🍈")

Rel(eco1, eco2, "collaborates with", "")
Rel(econ, eco1, "collaborates with", "")
Rel(eco1, econ1, "collaborates with", "")
Rel(eco2, econ, "collaborates with", "")
Rel(econ1, eco2, "collaborates with", "")
Rel(econ, econ1, "collaborates with", "")
@enduml
```

<p markdown="1" style= "text-align: center;">
Each ecosystem is governed by an authority which has specific **Objectives**, ie aim or goal, something to achieve.
<br/>:octicons-arrow-down-24:<br/>
Each **Objective** is translated into **Policies**, ie principles/ideas on how something is done, plan of action for decision making.
<br/>:octicons-arrow-down-24:<br/>
Each **Policy** is enforced with **Procedures and Rules**, ie orders/methods of doing something, permissions, prohibitions or duties.
<br/><br/>
</p>



<div markdown="1" style= "text-align: center;">
```mermaid
flowchart LR
    objective("Objectives")
    policy("Policies")
    rules("Procedures & Rules")
    objective --> policy --> rules
```
</div>

## :simple-handshake: Interoperability assessment

Given several ecosystems, there are different levels of interoperability and the first one is on the objectives.  

!!! success "Objectives before technology"

    It's a *sine qua non* condition for the ecosystems to have at least partial common objectives to be interoperable.


```mermaid
flowchart LR

    eco1((Ecosystem))
    eco2((Ecosystem))

    eco1 <-- "Polices assessment<br>⬇️" ---> eco2
    eco1 <-- "Procedures and rules assessment<br>⬇️" --> eco2
    eco1 <-- "Semantic assessment<br>⬇️" --> eco2
    eco1 <-- Technical assessment --> eco2
```

Assuming that there is **at least one common objective** between ecosystems, then the following assessment can be done, by order:

### Polices assessment

Comparing the principles and ideas on how something is done, of the plan of action for decision making.

### Procedures and rules assessment

Comparing for methodologies for doing something, the state machine model, of the permissions, prohibitions, duties.

### Semantic assessment

Comparing the information model, vocabularies and glossaries.


### Technical assessment

Comparing the protocols, data formats and architecture.

!!! failure "Less middleware platforms and toolboxes but more silos"

    The previously assessment tasks must be performed in the given order to avoid surprises later in the assessment such as compatibles software but contradicting business goals.  
    Different architecture models such as centralized, distributed, federated or decentralised can be made interoperable by developing additional gateways or proxies.  However the best technology in the world will never enable cross-ecosystem interoperability if there is no common objective.
