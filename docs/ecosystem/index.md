# :material-earth: Federation of Ecosystems

Banana-X 🍌 is both:

- a blueprint to build interoperable ecosystems
- a free and public ecosystem instance built out of its own blueprint

While this ecosystem focuses on data 💾 and edge to cloud infrastructure 🛣️, this blueprint can be used for other type of goods and fields such as Digital Product Passport[^dpp] for batteries 🔋, clothes 👕, cosmetics 💄, ...

[^dpp]: DPP in [Green Deal: New proposals to make sustainable products the norm and boost Europe's resource independence.](https://ec.europa.eu/commission/presscorner/detail/en/ip_22_2013)
