# :material-puzzle: Ecosystem building blocks

<!-- ```kroki-plantuml
@startuml Ecosystem Authorities
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Context.puml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml

System_Boundary(eco, "Ecosystem") {

    Container(catalogue, "Publication", "", "discoverability and advertising of goods and services")
    Container(identity, "Identity", "", "sources of identifiers")
    Container(semantic, "Semantic schema", "", "agreement about the intended meaning of given vocabularies")
    Container(registry, "Registry", "", "entry-point and reference source of information for the ecosystem")
    ' verification of identifiers, keys, and other relevant data, such as verifiable credential schemas, revocation registries, issuer public keys, and so on
    System_Boundary(contract, "Contracting") {
        Container(contract_framework, "Procedures and Rules")
        ' "framework for contracting"
        Container(contract_traceability, "Traceability")
        ' "confidential / immutable storage"
        Container(contract_enforcement, "Enforcement")
        ' "legal or technical monitoring of the obligations or means or results"
    }
        Container(policies_reasoning, "Policies reasoning")
        ' "semantic analysis of contract content"

}
@enduml
``` -->


<style>
    :root {
        --greenlight: #b3d89c;
        --greenmiddle: #70ad47;
        --greendark: #548235;

        --bluelight: #96b0de;
        --bluemiddle: #6a8ed0;
        --bluedark: #4472c4;
        --bordersize: 0.5em;
    }
    
    #bb {
        width: 100%;
        table {
            border: 0px;
        }
        .bb-light {
            text-align: center;
            font-weight: bold;
            padding: 0 var(--bordersize) 0 var(--bordersize);
        }
        .bb-middle {
            width: 33%;
            text-align: center;
            color: white;
            font-style: italic;
            font-weight: bold;
        }
        .bb-dark {
            font-style: normal;
            text-align: center;
            line-height: 1.5;
            padding-top: 0.2em;
            padding-bottom: 0.2em;
            height: calc(2rem * 1.5);
            cursor: pointer;
            p {
                padding-top: 1em;
                padding-bottom: 1em;
                padding-left: 0.2em;
                padding-right: 0.2em;
                border: 1px solid #ccc;
                background: #ffffff55;
            }
        }
    }

    .description-hidden {
        display: none;
    }

    div.bb-light:nth-child(1) {
        background-color: var(--greenlight);
        .bb-middle {
            background-color: var(--greenmiddle);
            border: var(--bordersize) solid var(--greenlight);
            .bb-dark {
                background-color: var(--greendark);
                border: var(--bordersize) solid var(--greenmiddle);
            }
        }
    }

    div.bb-light:nth-child(2) {
        background-color: var(--bluelight);
        .bb-middle {
            background-color: var(--bluemiddle);
            border: var(--bordersize) solid var(--bluelight);
            .bb-dark {
                background-color: var(--bluedark);
                border: var(--bordersize) solid var(--bluemiddle);
            }
        }
    }


</style>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        document.querySelectorAll(".bb-dark p").forEach(description => description.classList.add("description-hidden"));
        document.querySelectorAll(".bb-dark").forEach(bb =>
            bb.addEventListener("click", function() {
                let description = bb.querySelector("p");
                description.classList.toggle("description-hidden");
            })
        );
    });
</script>

!!! tip
    Click on each Building Block to discover what is covered.

<div id="bb">
    <div class="bb-light">
        Organisational and Business Building Blocks
        <table>
            <tr>
                <td class="bb-middle">Business
                    <table>
                        <tr><td class="bb-dark">Business Model Development
                        <p>Out of Scope.</p></td></tr>
                        <tr><td class="bb-dark">Use Case Development
                        <p>Out of Scope.</p></td></tr>
                        <tr><td class="bb-dark">Data Product Development
                        <p>Out of Scope.</p></td></tr>
                        <tr><td class="bb-dark">Data Space Intermediary
                        <p class=":material-arrow-expand:">Cover Service intermediaries which offer services packaged together. It includes dataspace intermediary services as services for data exchange from neutral providers.</p></td></tr>
                    </table>
                </td>
                <td class="bb-middle">Governance
                    <table>
                        <tr><td class="bb-dark">Organisational Governance
                        <p class=":fontawesome-solid-expand:">Cover the overall ecosystem governance policies, describing who is entitled to do what and how. (Ex:how are rules updated ?)</p></td></tr>
                        <tr><td class="bb-dark">Data Sharing Governance
                        <p class=":fontawesome-solid-expand:">Translation of the intent of the European data protection regulations into procedures and rules.</p></td></tr>
                    </table>
                </td>
                <td class="bb-middle">Legal
                    <table>
                        <tr><td class="bb-dark">Regulatory Compliance
                        <p class=":material-arrow-collapse:">Functional and information models of standard CASCO standards. Focus on the identification of the objects of conformity.</p></td></tr>
                        <tr><td class="bb-dark">Contractual Framework
                        <p>Foster adoption of common template, procedure and tracealibity patterns for contracts.</p></td></tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div class="bb-light">
        <table>
            <tr>
                <td class="bb-middle">Data Interoperability
                    <table>
                        <tr><td class="bb-dark">Data Models
                        <p>ontology aligments</p></td></tr>
                        <tr><td class="bb-dark">Data Exchange
                        <p>Focus on the negotiation phase of the exchange, where policy reasoning is performed. It includes license, terms of use, privacy notices, ...</p></td></tr>
                        <tr><td class="bb-dark">Traceability & Observability
                        <p>For the provider: data/services are being used as intented. For the consumer: the used data is curated and comes from legit sources.</p></td></tr>
                    </table>
                </td>
                <td class="bb-middle">Data Sovereignty & Trust
                    <table>
                        <tr><td class="bb-dark">Access & Usage policies and enforcement
                        <p>Cover the policy reasoning (PDP) and the technical enforcement (PEP) of the result of the negotiation: FHE, TEE, MPC, Federated Learning, Compute to Data, DRM, sandboxes, Remote Attestation.</p></td></tr>
                        <tr><td class="bb-dark">Identity & Attestation Management
                        <p>Cover Claim Management in general, which include claim about identities and rights delegations.</p></td></tr>
                        <tr><td class="bb-dark">Trust Framework
                        <p>Technical translation of the Governance Building Blocks with Trust Anchors being for Claim what TSP are for identities.</p></td></tr>
                    </table>
                </td>
                <td class="bb-middle">Data Value Creation
                    <table>
                        <tr><td class="bb-dark">Data Products, Services and Offerings Descriptions
                        <p>Define an ontology for describing Services and Resources onto which are attached policies for access and usage control.</p></td></tr>
                        <tr><td class="bb-dark">Publication and Discovery
                        <p>Describe once and advertise everywhere, while always being in control of with whom it's shared.</p></td></tr>
                        <tr><td class="bb-dark">Marketplaces
                        <p>Focus on Service Composition using DSL and RDF embeddings</p></td></tr>
                    </table>
                </td>
            </tr>
        </table>
        Technical Building Blocks
    </div>
</div>

<!-- ### Trust

Trust is a crucial factor when data is shared across numerous organizations, individuals and systems and is directly related to the capacity of the ecosystem authority to translate its objectives into actionable sets of policies, procedures and rules.

The participants in the ecosystem should feel confident that they are in full control over their own data and need assurances that other participants are who they claim to be, data is handled in accordance with the established rules and regulations, and the technology stack used is also secure and trustworthy.

This building block intends to address the following challenges:

- How claims made by participants can be validated and verified?
- How the identification of a participant/entity can be verified?
- How is it ensured that only authorized participants are allowed to access the data?
- How is it ensured that a claim sent by a participant to another participant has not been sent by a fraudster?
- How is it ensured that data is only managed as authorized?
- How is it ensured that data is proper for the intended usage? -->

<!-- 
#### Key capabilities

The Trust building block provides mechanisms to verify claims made by other parties. Such claims can relate to the quantity of available information in the claims, the ability to assess the veracity of the information provided or historical records of the provided information.

Independently of the type of architecture, centralised, decentralised, federated or distributed, this building block provides the data space with the following procedure and rules:

    the semantic to describe and verify the description of service, data products and policies, notably for access control, usage purpose, consent, authorisation and rights delegation

    the level of assessment activity - either a declaration or certification - for making specific claim.

    the list of accredited parties eligible to issue identifiers, certifications and cryptographic material such as certificates.

    the procedure for parties to request and issue identifiers for themselves, services or data products.

The recommendation is for the data space governance authority to expose those information under one Registry service.


--------

How to implement this building block?

 

Trust being first and foremost a risk assessment about the probability for someone to fulfil a promise or perform a task and given that there is at least one common objective shared across parties, it’s critical for the parties to align on common vocabularies and models representing their business knowledge.

As such, Trust can be modeled as 2nd order logic rules on an ontology with one or more services leveraging the use of:

    Shape (SHACL or ShEx) to enforce a semantic description of, e.g., data and service offerings (cf. the 

     building block)

    reasoning rules or query expression (e.g., SPARQL) to verify relations between claims describing entities

    list of Trust Service Providers and Trust Anchors to identify the certificate issuers and entitled parties to sign claims.

    list of accredited service endpoints providing the other building block capabilities - catalogue, Policy Decision Point, data translation logging, …

Those information can be accessed via the Registry service 

 

One important aspect of this building block is the omnipresent use of asymmetric cryptography to ensure, depending of the case:

    privacy of the exchanged payload/data/claim with encryption

    integrity of the exchanged payload/data/claim with signature

 

Some examples of the implementation of this building block are the following:

The Gaia-X Trust Framework based on EU values and principles and has the purpose of providing a common governance that results in a basic level of interoperability across individual data spaces while letting the users in full control of their choices , eIDAS regulation (see Regulation on electronic identification and trust services for electronic transactions in the internal market) and TRAIN (TRust mAnagement INfrastructure) which provides a solution to enable trust in a specific data space.

Other examples in sectorial or regional contexts are represented respectively by the Open Credentialing Initiative and the government-driven initiative Pan-Canadian Trust Framework (PCTF- Pan-Canadian Trust Framework).


---

Links to relevant documents

 

 

 

      - Relation between ISO/IEC 17000:2020 Conformity Assessment and W3C Verifiable Credentials.

Standards and Reference Implementations
Standards

eIDAS - 

 

ISO/IEC 17000:2020 https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en

Shacl shape - 

 

ShEx - 

 

SPARQL - 

 

OWL ontology - 

 

W3C Verifiable Data Registry - 

     

Reference Implementations

  - Open source implementation of the Gaia-X Registry

      - API specifications of the European Blockchain Service Infrastructure

 

 


--------- old


Introduction

This section provides an introduction to the functional specifications of the “Trust building block” within the technical pillar “Data sovereignty and Trust”

Trust is a crucial factor when data is shared across numerous organizations, individuals and systems. The participants in data spaces should feel confident that they are in full control over their own data and need assurances that other participants are who they claim to be, the descriptions (or claims) they provide are trustworthy and that they comply with defined rules/agreements and governance framework. The concept of “trust” then can be translated into compliance to a set of rules, verified through a technology framework, that provides evidence of the level of transparency, security, controllability, and interoperability.

At the root of the “Trust building block”, are the Trust Anchors (entities endorsed by the data space governance authority)  to ensure the authenticity, integrity and reliability of data and data transactions within the data space, as trust anchors validate the claims made by the participants, such as an entity is in fact a real legal entity and/or self-descriptions of the participants service offerings and resources. Trust anchors issue cryptographic keys to the participants in the form of “Verifiable Credentials” (VCs). Verifiable Credentials are key components of secure data and identity systems, enabling the issuance and presentation of trustworthy and tamper-proof credentials.

Moreover, any data space requires a “Trust Framework”, i.e. a composition of policies, rules, standards, and procedures designed for trust decisions in data spaces based on verifiable credentials to enable trusted and secure data transactions, while fostering data sovereignty. A trust framework is part of the data space governance framework. Thus, one of the key functionalities of a Trust Framework is to describe how claims are signed and how these signatures can be validated and verified by Trust anchors.

Trust is also built on what it is called “trust services”, i.e. “ data space enabling services that offer assurances within a data space”. Examples include digital certification services, e.g. for verifying product authenticity, or certifying the processes of data provisioning services Trust Services are provided by entities that are widely recognized and have a high level of credibility, e.g. the list of qualified trust services providers issued by the Member States of the EU/EEA.

In terms of references, the proposed approach is in line with the Gaia-X Trust Framework. Another relevant reference in the context of defining a Trust Framework with regard to general trust models is represented by eIDAS regulation (see Regulation on electronic identification and trust services for electronic transactions in the internal market), while TRAIN (TRust mAnagement INfrastructure) provides a solution to enable trust in a specific data space.

Examples of the definition of Trust Frameworks in sectorial or regional contexts are represented respectively by the Open Credentialing Initiative and the government-driven initiative Pan-Canadian Trust Framework (PCTF- Pan-Canadian Trust Framework).

Interrelationships: Dependencies and relationships (boundaries to be clarified). The Trust Building Block is interrelated with the majority of the technical building blocks, particularly with the technical building block “Identity Management” and the “Governance building blocks”, but also interconnections  exist with the other technical building blocks as described below.

Figure C.13: DSSC Building Blocks taxonomy

Governance building blocks: Setting up a “trustworthy” governance framework is a fundamental step for the implementation of data spaces in order to establish adequate rules and agreements, roles, responsibilities and settling of disputes.  Moreover, Trust frameworks take into account legal, regulatory and compliance requirements, such as data sovereignty regulations, intellectual property rights, and domain-specific regulations.. By making this process transparent and fair, it clearly contributes to establish “trust” and provide the participants with the required level of confidence, e.g. that they are in control over the data they share, who has access to data, how is secured and ensure compliance with relevant legislation.

Identity Management: The identity management building block allows identification, authentication, and authorization of stakeholders operating in a data space. It ensures that entities, individuals, machines, and other actors are provided with acknowledged identities, and that those identities can be authenticated and verified. Trusted and reliable information of the participants in the data space is essential to verify the real (legal) identity of an entity when interacting with data space participants (transaction participants). There should be no lock-in to any implementation of identity system and, after trust is established, underlying existing technologies already in use by participants can be reused. Recommendations: Use of Trust participants lists issued by EU MS with Trust Service Providers (TSPs). Thus, components implementing Identity and Access Management (IAM) functions enable to ensure that data is accessed by trusted parties and that defined access policies are enforced. As such, the creation of federated and trusted identities in data spaces can be supported by European regulations such as eIDAS.

Marketplace & Usage Accounting: The use of trust services for electronic transactions by both private and public entities facilitates cross-border transactions, highly relevant for marketplaces. This can be achieved, by combining digital certificates issued by EU Trust Service Providers (TSPs) and verifiable credentials which is fully aligned with eIDAS regulation.

Data Exchange: Ensuring fair, secure and trustworthy data sharing is at the core of data spaces. Trust frameworks and data exchange in data spaces are closely interdependent because trust frameworks provide the foundation for establishing trust and confidence among data stakeholders, which in turn enables effective and secure data exchange. In order to enable data exchange services, the organizations need to make available their data in machine-readable formats, thus the use of agreed data exchange protocols (APIs) is required. Data spaces might also leverage on the use of blockchain related technologies to ensure secure and trusted data exchange, use Verifiable credentials, or data traceability (e.g. EBSI- European Blockchain Services Infrastructure). These protocols play a key role to achieve interoperability, i.e. some of them enable interoperability across domains within a data space or across data spaces, e.g. ETSI NGSI-LD API (domain-agnostic), in addition to specific domain ones. Moreover, these protocols can incorporate functionalities that serve to establish or reinforce ‘trust’ among the participants:

o   Security: protocols can provide security measures, such as encryption or secure authentication methods to ensure data is transferred in a secured way, thus contributing to reinforce ‘trust’ as it provides a high level of assurance that sensitive commercial data or personal data remains confidential.

o   Authentication and Authorization: Data exchange protocols might include metadata e.g. for authentication and authorization. Trust and transparency are enhanced by the use of these protocols to facilitate the identification of data space participants and ensure that they have the necessary permissions to access and exchange data.

Access Control and Authorization: Trust frameworks define the rules and mechanisms for authentication and authorization within a data space. These access control mechanisms are crucial for regulating who can access and exchange data. Without proper authorization based on trust framework guidelines, data exchange can lead to unauthorized access or data breaches.

Access and usage policies: The adoption of a common trust framework, implies the enforcement of policies agreed upon, e.g. who can access the data and services, what are the processing and security mechanisms in place . The enforcement of these policies will also impact on enhancing the level of trust among the data space participants.

Marketplace & Usage Accounting: The use of trust services for electronic transactions by both private and public entities facilitates cross-border transactions, which is highly relevant for marketplaces. This can be achieved, by combining digital certificates issued by EU Trust Service Providers (TSPs) and verifiable credentials which is fully aligned with eIDAS regulation.

Moreover, there is also a strong link between the “Trust building block” and the so-called "Data Space Registries” for the identification of trusted data space participants which are handled by the data space governance authority based on the established rules. This Data Space Registry can be public or private and contains information to support the overall trust in a data space (e.g. list of recognized trust anchors, valid public keys, etc). This registry can be realized based on a centralized approach (e.g. IDS-RAM), decentralized or federated approaches (e.g. Gaia-X provides guidelines for the three different approaches).
Purpose of the Building Block

Data ecosystem parties that commit to contribute to a data space initiative need to adopt clear and trustworthy data governance mechanisms. The Trust framework defines a set of rules and specifications (e.g. standards, legislation, guidelines, etc) that different parties agree to follow to be part of a data ecosystem. These rules provide a common trustworthy governance and the basic level of interoperability across data space initiatives while letting the users in full control of their choices. This process facilitates the data transactions or sharing of information with other data ecosystem parties, as they can use their digital identities and attributes in a consistent, fair and trusted manner, unlocking the value of (cross-sectoral) data sharing. Ultimately, trust frameworks not only serve to increase trust but also to avoid market fragmentation and increase economic and social benefits.
Purpose and problem

From the operational point of view, any participant within a data space can make claims about other entities and issue self-signed proofs. However, without a structured common system of underlying trust chains, these proofs would be of no utility. Therefore, within a dataspace a trust anchor is needed to validate matters such as credentials used for identification, self-descriptions of data & services and whether an entity belongs to a specific data space. Such functionality is essential for other participants in the data space to be able to work with a potential unknown or (initially) untrusted entity. The proof of validation of claims made by the participants in the data space will be technically realized through verifiable credentials (e.g. W3C Verifiable Credentials Data Model)

Trust anchors can be bodies, e.g. CABs (Conformity Assessment Bodies) or technical means accredited by the data space governance authority to be trustworthy anchors in the cryptographic chain of keypairs used to digitally sign claims about an object.

The role of Trust Anchors is vital in building and maintaining trust in data spaces by ensuring the authenticity, integrity, security and reliability of data and data transactions within the data space.
The list of valid Trust Anchors is stored in the data space registry.

    See below some examples of the type of problems this building block can solve:

o Trust frameworks establish the common set of rules that apply to all participants regardless of their specific domain needs, local regulatory frameworks, or industry boundaries, for example, different service providers ranging from public sectors to private sectors, all follow the same data sharing practices under a trust framework, avoiding this way the need for bilateral agreements.

o Generally, identity management was following a centralized approach, however with the emergence of data spaces, a decentralized identity management approach is more suitable. As such, a Trust framework and a Decentralized Identity and Access Management enable trusted operations of data spaces without a central intermediary for all the interactions among the data space participants.
Use cases

There are various ways in which the building block can be used to solve specific problems or achieve certain goals in the context of data spaces. A non-exhaustive list contains the following use cases:

o Verification of the veracity of the attributes as part of self-descriptions

o Creation of federated and trusted identities in data spaces (eIDAS regulation)

o Verification of the self-signed set of claims made by participants/entities

o Verify that an entity (represented by an identifier) is a real legal entity

Note: more use cases and potential barriers that this BB cotributes to overcome are described in subsequent sections.
Suggested solution

The adoption of a trust framework can solve the following issues/barriers:

    Verify that an entity (represented with an identifier for online processes) is a real valid legal entity (“ID Binding”).

    Facilitate interoperable cross-border data-related transactions by combining digital certificates issued by Trust Service Providers (TSPs) authorized by EU MS.

    Boost the EU data economy by using recognized electronic identification. The use of trust services for electronic transactions by both private and public entities facilitates the interoperability of Verifiable Credentials (VC) across EU markets (aligned with the eIDAS Regulation)

    Verify that an entity of a data ecosystem can be trusted (“Proof of participation”), e.g. product offerings.

    Provides security countermeasures to prevent data tampering, as it is cryptographically signed.

    Verification of the self-signed set of claims made by participants/entities.

    Allow data spaces governance authorities and DGA compliant- Data Intermediaries to establish their own governance framework.

    Proof of Issuing Authority. This provides the means to verify the verifiable credentials provided by a participant, have been issued by a trusted issuer.

    Support the verification of a multitude of verifiable credentials, e.g. identity is one of the VCs of a natural person, an entity or a machine. In business, other claims related to that entity are also very important, e.g. a standard certification of an organization or a service, etc.

Key stakeholders

    Data ecosystem participants (Consumer, Federator, Provider)

    Data Space Governance Authority: accountable for a particular governance scope and responsible of running the necessary governance processes, without replacing the role of public enforcement authorities.

    Trust Anchor: recognized entity which holds information about each participating party and allows it to check for the admittance of each party. Within the Gaia-X context, Trust Anchors are bodies, parties, ie CAB (conformity assessment body) or technical means accredited by the bodies of the Gaia-X association to be trustworthy anchors in the cryptographic chain of keypairs used to digitally sign statements or claims about an object.

    Trust Service Providers: the Member States of the European Union and European Economic Area (EU/EEA) publish trusted lists of qualified trust service providers in accordance with the eIDAS Regulation.

    Trusted Issuer: it is a trusted public entity which can issue Verifiable Credentials belonging to a
    given domain or of a given type.

Definitions and Vocabulary

Term
	

Description

ID Binding
	

It refers to the link between an identifier (used to represent an entity - it can contain metadata) and the real legal identity of an entity. It is a fundamental functionality to verify that an entity (represented by an identifier in online processes) is a real legal entity.

LOTL (List of Trusted Lists)
	

it refers to the list of qualified trust service providers in accordance to the eIDAS Regulation published by the Member States of the European Union and the European Economic Area.

W3C Verifiable Credentials[1]:

 

 

 
	

W3C Verifiable Credentials are a set of standards and protocols created by the World Wide Web Consortium (W3C) for creating, issuing, and verifying digital credentials in a decentralized and secure manner.

W3C DID[2]

 

 

 
	

Decentralized Identifiers (DID) is an official web standard. DIDs are cryptographic digital identifiers not tied to any central authority. DIDs can represent individuals, organizations, online communities, governments, etc

eIDAS Regulation
	

The EU Regulation on electronic identification and trust services for electronic transactions in the internal market

VerifiableID
	

It refers to a type of Verifiable Credential that a natural person or legal entity can use to demonstrate who they are. This verifiableID can be used for Identification and Authentication. (ref. Verifiable Attestation for ID - EBSI Specifications - (

 ))

Issuer
	

It refers to a party that creates and issues Verifiable Credentials

Verifier
	

It refers to a party who requests/verifies Verifiable Credentials (e.g. Verifiable IDs or Verifiable attestations), such as to provide a service.

Self-description
	

It refers to the combination of Verifiable Credentials associated to characteristics of a (Data) Product, Service or Resource which get the form of a Verifiable Presentation also referred as Self-Description.

Data Space Registry
	

It is a public or private registry where specific information about entities is stored. There are several types: central, decentralized models or a federated approach.

ELSI
	

ETSI Legal Semantic Identifier

 

[1] 

[2] 
Conceptual Model
Figure 2: Conceptual Model - Trust Services

Generally, each data space has to be interoperable but at the same time needs to adapt to their specific domain needs and governance framework as well as market regulations, thus the trust framework provides a set of rules and specifications that can be utilized by i) Data space governance authorities, such as data intermediaries from the Data Governance Act to build their governance mechanisms and ii) any federation seeking interoperability an technical compatibility of their services.

From the practical implementation point of view, the conceptual model of this trust building block can be summarized as the following basic steps: 1) an applicant/entity applies to become part of a data space (pre- onboarding) providing the required identity and related attributes for further verification (self-description); 2) this self-description follows the Linked Data Principles to describe attributes, this is known as Verifiable credentials to build a decentralized knowledge graph; 3) to verify and store the claims made by the applicant, there will be a process to determine the authenticity and the identification of the applicant, where a trust anchor plays a key role to verify the veracity of such information which is validated and signed by trusted entities in the data space registry (e.g. conformity assessment bodies- CABs), thus allowing the exchange of trusted information via the exchange of credentials within and across dataspaces.

Practical example, the operationalization of the Trust Framework in Gaia-X can be represented as follows (see Figure 3):
Figure 3: Gaia-X Digital Clearing House (GXDCH)

A GXDCH is a node of verification of the Gaia-X rules. It is the place to go to obtain Gaia-X compliance and become a part of the Gaia-X ecosystem. There are multiple nodes operated by service providers acting as federators. The GXDCH services are under Gaia-X AISBL governance. Operating a GXDCH is open to all service providers who comply to the GXDCH operation rules.

A list of the current range of the GXDCH components and their classification into mandatory and optional ones can be found below:

GXDCH Components*
	

mandatory
	

optional

Gaia-X Registry
	

x
	

 

Gaia-X Compliance
	

x
	

 

Wizard
	

 
	

x

(but extremely useful)

Catalogue
	

 
	

x

(but extremely useful)

Organisation & Personal Credential Manager
	

 
	

x

(but extremely useful)

Notarisation Services
	

 
	

x

(but extremely useful)

*current state, might be extended in the future
Functionalities

A Trust framework defines and enforces a set of rules that data ecosystem parties agree to comply with, allowing them the use of their digital identities in a transparent and trusted way, facilitating the sharing of information and transactions with other participants. The trust framework can be implemented via the W3C Verifiable Credentials Model, using a Verifiable Data Registry to query and store the information in a machine-readable format and provide adequate mechanisms to validate and verify the claims of the participants at any time. Trust anchors play a fundamental role in data spaces to process and validate the claims made by participants, and provide the assurance needed to establish trust among participants.

The W3C Verifiable Credentials Data Model (Verifiable Credentials Data Model v1.1 (w3.org)) relies on three data model concepts, i.e. claims, verifiable credentials and verifiable presentations, which form the foundation of this specification:

Claims: these are statements made about entities that cannot be verified, i.e. to be able to trust claims, more information is needed.

Verifiable Credentials (VCs): each VC contains claims made by the same entity, and might include metadata and an identifier to describe the issuer, a public key to be used for verification, expiration date, etc. As for example: digital birth certificates, digital educational certificates, etc. Thus, a verifiable credential is a set of tamper-evident claims and metadata that cryptographically prove who issued it.

Verifiable presentations (VPs): VPs contain one or more VCs and the issuer is verifiable. Generally, presentations are often related to the same subject but might have been issued by multiple issuers.

The serialization format of Verifiable Presentations and Verifiable Credentials is JSON-LD.

VCs/VPs can be also encoded as JWT (Jason Web Token).

Generally, self-descriptions are used to describe entities, products or services in the form of verifiable credentials and are characterized by the following functionalities:

    machine- readable formats

    cryptographically signed, preventing tampering with its content

    following the Linked Data principles[3] to describe attributes.

Identity Binding

Any trust framework should provide the means to verify the identity of a real legal entity which is represented via an identifier in online processes. This link between the legal entity and the identifier is called “ID Binding”.

It is recommended to rely on identifiers already used in digital certificates issued by Trust Service Providers (TSPs) authorized by EU MS. The process of ID Binding based on these TSPs, if adopted by both public and private sectors will improve interoperability in data spaces of data ecosystems, facilitating data-driven businesses across the EU.

The combination of digital certificates issued by TSPs, and Verifiable Credentials contributes to the legal validity and interoperability of the cross-border data-related transactions in the European Union facilitating the cross-border validation of eSignatures, eSeals, etc. Generally, Verifiable Credentials and Presentations (including Product Specifications and Service Offerings) used in data ecosystem will be signed using digital certificates using the JAdES format as defined in ETSI TS 119 182-1 (Electronic Signatures and Infrastructures (ESI); JAdES digital signatures)
ID Binding and the Verifiable Credential

The process of verifying that a Verifiable Credential sent online by a participant is in fact an entity authorized to do this and not a fraudulent entity. This process is called ID Binding of the credential (VerifiableID). There are several means to implement this process by leveraging on the digital certificates issued by TSPs and public key cryptography.
Decentralized Identifiers (DIDs) – Identifiers for legal persons

 The use of W3C VCs with DIDs as identifiers (official web standard) to provide organizations and individuals with greater privacy and security. DIDs are cryptographic digital identifiers not tied to any central authority and can be stored and transferred across different types of digital infrastructures, such as blockchain.

Generally, data ecosystems can adopt the use of one or more DID methods and associated DID resolution mechanisms (e.g. did:web, did:peer) and for legal persons, it is recommended to use the identifiers from the digital certificates issued by TSP that comply with ETSI standards adopting the same nomenclature (e.g. ELSI- ETSI Legal Semantic Identifier).

In data spaces, a legal person can use this function to obtain a DID identifier without the need to generate new identifiers.
Identifiers for gateways and connectors

When referring to identifiers for gateways such as IDS connectors, the required identification process is similar to the identification process for legal persons, where the application must be linked to a legal person or natural person identifier to determine the delegation of power to the application context. Such identifiers could be realized as X.509 certificates or as DID. The current version of the IDS-RAM[4] describes the use of X.509 certificates.

Moreover, leveraging on the Trust framework, a decentralized and access management enable transactions in the marketplace, address the following issues/barriers:

 

    Verify the identification of the participants, i.e. an identifier (entities are represented by an identifier) sent by a participant to another entity has been sent by the participant and not by an fraudster that knows about the identifier. In addition, the identifier should be cryptographically bound to the verifiable credentials sent by the participant, so the attributes/facts attested in those credentials can be used for Authentication and Authorization.

    Advanced identity management for access control and policy enforcement, i.e. authorization based on both RBAC (role-based access control) and ABAC (attribute-based access control) based on the attributes/facts attested in the verifiable credentials provided by a participant.

Open Challenges – Future Roadmap

Use of digital certificates issued by the EU TSPs by both private and public entities participating in data spaces when performing the ID Binding (particularly during onboarding processes in data spaces and verification of signatures of credentials).

Additional considerations will be needed when the upcoming eIDAS 2.0 comes into force.
Conclusions

Data spaces should bring the rules and technical means to manage the identify of participants, the verification of their claims and the enforcement of policies to access and use data in order to ensure data sovereignty and trust among the data space participants

 
 -->
