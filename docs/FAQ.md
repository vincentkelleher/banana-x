# Frequently Asked Questions

While all the answers below could be open for debate, the intend of this section is to give direct, easy to understand replies that address the common use cases.

??? note "What is Trust ?"

    It's a risk assessment about an entity capable to fullfil a promise, based on:

    - the quantity of available information about the entity
    - the capacity to assess the veracity of the information
    - the historical records of the information

??? note "Do I need a dataspace to exchange data ?"
    No.  
    For convenience, the notion of dataspace - defined as a set of entities with common procedures and rules for sharing data - is useful in the early phase when writing the business plan.  
    Once the business plan in known, the contractualisation procedures will prevail.


??? note "Do I need a federation to use cloud service ?"
    No.  
    For convenience, the notion of federation - defined as a set of entities with interoperable service endpoints - is useful in the early phase when writing the architecture.  
    Once the architecture in known, the technical interoperability will prevail.
