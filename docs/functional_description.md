# What is the solution
In the section, the aim is to describe functionaly what the solution is.

## General description
As said in the introduction, the solution provides concepts and components in order to : 
- building a global network of catalogues for services and data products.
- enabling trusted data transaction at scale
- setting up interoperable governance across different domain(s).

To do so, the following steps have to be performed :
- [Participants](ecosystem.md) have to assessed by the Authority
- Synchronised catalogues have to be available to expose services and data products ressources
- [Providers](ecosystem.md) can publish services or data products on synchronised catalogues
- [Consumers](ecosystem.md) can search and share a contract with the Provider

>> insert global schema

## Participant onboarding
All Participants, in order to interact within the Authority, have to be declared and assesed by the Authority.
Indeed, the basic principles of trust rely on : 
- the capacity from the Particpant to provide a proof to be assessed
- each proof has to be certified by a Trust Anchor or a Notary, also assessed by the Autority
- as a result, the Authority will give the Participant a proof of assessment

To do so, interactions will rely on [Verifiable Credentials](fundamentals.md#verifiable-credentals), aka VC : 
- a participant VC
- an Authority VC when the assessment is performed

The use of a Credential Manager to store the VCs is more than encouraged.

## Synchronised Catalogues

## Service and Data Product share

## Search of service or Data Product and contract