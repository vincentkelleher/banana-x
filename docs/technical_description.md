# How is the solution working
In the section, the aim is to go deeply in the solution, aka Authority Digital Clearing House, description all components.

## Architecture schema

## Mandatory Components

### Compliance

### Registry

### Notarisation Services

### Credential Events service

### InterPlanetary File System (IPFS) node

### Logging service

## Optional Components

### Wizard

### Catalogue

### Key Management System

### Policy Decision Point 

### Data Exchange services