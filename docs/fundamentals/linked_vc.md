# :material-shield-link-variant: Linked Verifiable Credentials

The usage of Linked Data inside one Verifiable Credential is well described in the W3C VC specifications.

The usage of Linked Data across several Verifiable Credentials to build a decentralised machine readable knowledge graph brings new challenges.

The solution selected is to include in the VP all the needed VC.  

**Pros**: This enables the use of IDs only known and resolvable from within the set of presented VC and always have the claims associated with its encapsulating credential.  
**Cons**: This approach might create a very big VP request which can be balanced using compression algorithms.


!!! example "2 linked Verifiable Credentials"

    When a verifier requests the `VC1`, the holder should embed in the `VP1`, all the VCs refered in the `VC1's credentialSubject` object and recursively, ie `VC2` here.  
    URI are not necessarily resolvable.

    ```json
    {
      "@id": "https://example.com/VP1",
      "verifiableCredential": [
        {
          "@id": "https://example.com/VC1",
          "credentialSubject": {
            "@id": "https://example.com/VC1/CS1",
            "hasProperty": {"@id": "VC2/CS1"}
          }
        },
        {
          "@id": "https://example.com/VC2",
          "credentialSubject": {
            "@id": "https://example.com/VC2/CS1"
          }
        }
      ]
    }
    ```
